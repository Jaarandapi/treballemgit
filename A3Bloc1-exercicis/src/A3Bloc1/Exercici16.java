package A3Bloc1;
import java.util.Scanner;
/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 16
 * Descripci�:Algorisme que llegeix una qualificaci� num�rica v�lida i escriu com a resultat la mateixa qualificaci� per� alfab�ticament. 
 */
public class Exercici16 {

	public static void main(String[] args) {
		
		double num = 0;
		
		Scanner reader = new Scanner (System.in);
		System.out.println("Introdueix una nota ");
		num= reader.nextInt();
		
		if (num <= 1.5 ) {
			System.out.println("Molt deficient ");
		}
		if (num > 1.5 && num <= 4) {
			System.out.println("Insuficient ");
		}
	}

}
