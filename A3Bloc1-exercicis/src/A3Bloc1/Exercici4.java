package A3Bloc1;

import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 4
 * Descripci�: Ampliaci� de l�exercici 3 per�, tractant el cas de que siguin iguals.  
 * Autor: Jara Aranda
 */
public class Exercici4 {

	public static void main(String[] args) {
		int num = 0;
		int num2 = 0;
		Scanner reader = new Scanner(System.in);
		System.out.println("Introdueix un n�mero:");
		num = reader.nextInt();
		System.out.println("Introdueix un altre n�mero");
		num2 = reader.nextInt();
		if (num > num2)
		{
			
			System.out.println(num+" �s m�s gran que "+num2);
		}
		else if (num < num2)
		{
		System.out.println(num2+" �s m�s gran que "+num);
		}
		else
		{
			System.out.println(num+" es igual que "+num2);
		}
		reader.close();
	}

}
