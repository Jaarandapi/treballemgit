package A3Bloc1;
/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 1
 * Descripci�: Algorisme que llegeix un n�mero per teclat, el multiplica per 3 i mostra el resultat  
 * Autor: Jara Aranda
 */
import java. util.Scanner;

public class Exercici1 {

	public static void main(String[] args) {
		
		
		int num = 0;
		Scanner reader = new Scanner(System.in);
		
		System.out.println("introdueix un numero:");
		num = reader.nextInt();
		
		System.out.println("El resultat de multiplicar " + num + " per 3 �s " + num * 3);
		
		reader.close();
	}

}
