package A3Bloc1;
import java.util.Scanner;
/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 14
 * Descripci�: Algorisme que llegeix una quantitat per teclat i si �s superior a 500 li fa el 10% de descompte, en cas contrari �nicament li fa el 5%. El resultat cal que ens digui la quantitat descomptada i la quantitat a pagar.
 */
public class Exercici14 {

	public static void main(String[] args) {
		double preu = 0;
		double desc = 0;
		
		Scanner reader = new Scanner (System.in);
		System.out.println("Introdueix un preu ");
		preu= reader.nextInt();
		
		if (preu >500) {
			desc = preu * 10 /100;
			System.out.println("S'aplicar� 10% de descompte");
		}
		else {
			desc = preu * 5/100;
			System.out.println("S'aplicar� 5% de descompte ");
		}
		System.out.println("El preu total amb descompte "+(preu-desc));
		
	}
}
