package A3Bloc1;
import java.util.Scanner;
/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 15
 * Descripci�: Algorisme que llegeix dos n�meros en dos variables i intercanvia el seu valor. 
 */
public class Exercici15 {

	public static void main(String[] args) {
		int num1 = 6;
		int num2 = 8;
		int numX = 0;
		
		numX = num1;
		num1 = num2;
		num2 = numX;
	}

}
