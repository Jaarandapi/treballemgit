package A3Bloc1;

import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 3
 * Descripci�: Algorisme que llegeix dos n�meros i mostri com resultat qui �s el m�s gran. 
 * Autor: Jara Aranda
 */
public class Exercici3 {

	public static void main(String[] args) {
		int num = 0;
		int num2 = 0;
		Scanner reader = new Scanner(System.in);
		System.out.println("Introdueix un n�mero:");
		num = reader.nextInt();
		System.out.println("Introdueix un altre n�mero");
		num2 = reader.nextInt();
		if (num > num2)
		{
			
			System.out.println(num+" �s m�s gran que "+num2);
		}
		else
		{
		System.out.println(num2+" �s m�s gran que "+num);
		}
		reader.close();
	}

}
