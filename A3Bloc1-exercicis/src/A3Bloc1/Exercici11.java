package A3Bloc1;
import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 11
 * Descripci�: Algorisme que llegeix un n�mero i ens diu si �s parell o senar.  
 * Autor: Jara Aranda
 */
public class Exercici11 {

	public static void main(String[] args) {
		int num = 0;
		
		Scanner reader = new Scanner (System.in);
		System.out.println("Insereix una xifra ");
		num = reader.nextInt();
		if (num %2 == 0) {
			System.out.println(num+" �s parell ");
		}
		else {
			System.out.println(num+" �s senar ");
		}
	}

}
