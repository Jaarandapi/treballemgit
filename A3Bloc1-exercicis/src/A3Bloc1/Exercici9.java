package A3Bloc1;
import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 9
 * Descripci�: Algorisme que llegeix un n�mero i ens diu si �s m�ltiple de dos o de tres o de cap d�ells. 
 * Autor: Jara Aranda
 */
public class Exercici9 {

	public static void main(String[] args) {
		int num = 0;
		Scanner reader = new Scanner (System.in);
		
		System.out.println("Introdueix un n�mero ");
		num = reader.nextInt();
		if (num % 2 == 0)
		{
			System.out.println(num+" �s multiple de dos ");
		}
		else if (num % 2 != 0)
		{
			System.out.println(num+" no �s m�ltiple de dos ");
			
		if (num % 3 == 0)	
		{
			System.out.println(num+" �s m�ltiple de tres ");
		}
		if (num % 3 != 0)
		{
			System.out.println(num+" no �s m�ltiple de tres ");
		}
		}
		reader.close();
	}

}
