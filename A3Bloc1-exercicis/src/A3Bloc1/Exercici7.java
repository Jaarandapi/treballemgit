package A3Bloc1;

import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 7
 * Descripci�: Algorisme que llegeix dos n�meros i calcula la suma, la resta, la multiplicaci� i la divisi� reals. 
 * Autor: Jara Aranda
 */
public class Exercici7 {


	public static void main(String[] args) {
		float num = 0;
		float num2 = 0; 
		Scanner reader = new Scanner (System.in);
		System.out.println("Introdueix un n�mero ");
		num = reader.nextFloat();
		System.out.println("Introdueix un segon n�mero ");
		num2 = reader.nextFloat();
		System.out.println(" Suma "+num+" + "+num2+" fan "+(num+num2));		
		System.out.println(" Resta "+num+"-"+num2+" fan "+(num-num2) );
		System.out.println(" Multiplicaci� "+num+"*"+num2+" fan "+(num*num2) );
		if (num2 != 0){
		System.out.println(" Divisi� "+num+"/"+num2+" fan "+(num/num2) );
		}
		else 
		{
		System.out.println("Meeeec! ERROR" );
		}
		reader.close();
	}
}
