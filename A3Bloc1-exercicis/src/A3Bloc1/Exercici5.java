package A3Bloc1;

import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 5
 * Descripci�:Algorisme que llegeix 3 n�meros i digui quin �s el major.   
 * Autor: Jara Aranda
 */
public class Exercici5 {

	public static void main(String[] args) {
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		Scanner reader = new Scanner (System.in);
		System.out.println("introdueix un n�mero ");
		num = reader.nextInt();
		System.out.println("introdueix un segon n�mero ");
		num2 = reader.nextInt();
		System.out.println("introdueix un tercer n�mero ");
		num3 = reader.nextInt();
		if (num > num2 && num > num3)
		{
			System.out.println(num+" �s m�s gran que "+num2+ " i "+ num3);
		}
		else if (num2 > num && num2 > num3)
		{
			System.out.println(num2+" �s m�s gran que "+num+" i "+num3);
		}
		else
		{
			System.out.println(num3+" �s m�s gran que "+num+" i "+num2);
		}
		reader.close();
	}

}