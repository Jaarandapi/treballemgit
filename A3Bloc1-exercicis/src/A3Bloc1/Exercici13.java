package A3Bloc1;
import java.util.Scanner;
/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 13
 * Descripci�: Algorisme que llegeix 3 valors corresponents als coeficients A, B, i C d�una equaci� de segon grau i ens diu la soluci� aplicant la f�rmula de resoluci�.  
 * Autor: Jara Aranda
 */
public class Exercici13 {

	public static void main(String[] args) {
		int a = 0;
		int b = 0;
		int c = 0;
		double x1 , x2, arrel;
		Scanner reader = new Scanner (System.in);
		System.out.println("Introdueix un n�mero pel valor a ");
		a= reader.nextInt();
		
		System.out.println("Introdueix un n�mero pel valor b ");
		b= reader.nextInt();
		
		System.out.println("Introdueix un n�mero pel valor c ");
		c= reader.nextInt();
		reader.close();
		arrel = ((b*b) - (4*a*c));
		
		if (a!= 0 && arrel >0) {
			arrel = Math.sqrt(arrel);
			x1= (-b+ arrel)/(2*a);
			x2 = (-b- arrel)/(2*a);
			System.out.println("El resultat de x1 �s : "+x1);
			System.out.println("El resultat de x2 �s : "+x2);
					}
		else {
			System.out.println("L'equaci� no t� soluci�");
		}
			
	}

}
