package A3Bloc1;

import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 2
 * Descripci�: Algorisme que llegeix un n�mero i compara si �s major que 10. 
 * Autor: Jara Aranda
 */
public class Exercici2 {

	public static void main(String[] args) {
		int num = 0;
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Introduueix un n�mero:");
		num = reader.nextInt();
		if (num >10)
		{
			System.out.println(num+" �s m�s gran que 10");
		}
		else 
		{
			System.out.println(num+" �s m�s petit que 10");
		}
		reader.close();
	}
}