package A3Bloc1;
import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 8
 * Descripci�: Algorisme que calcula la superf�cie d�un rectangle. 
 * Autor: Jara Aranda
 */
public class Exercici8 {

	public static void main(String[] args) {
		float num = 0;
		float num2 = 0;
		Scanner reader = new Scanner (System.in);
		System.out.println("Introdueix l'al�ada ");
		num = reader.nextFloat();
		System.out.println("Introdueix la base ");
		num2 = reader.nextFloat ();
		if(num >0) {
			
		System.out.println("La superf�cie es..."+(num*num2));
		}
		else 
		{
			System.out.println("Imposibru");	
		}
		reader.close();
	}

}
