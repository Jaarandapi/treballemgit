package A3Bloc1;
import java.util.Scanner;

/*Institut Sabadell.
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 10
 * Descripci�: Algorisme que llegeix dos n�meros enters positius i diferents i ens diu si el major �s m�ltiple del menor, o el que �s al mateix, que el menor �s divisor del major. 
 * Autor: Jara Aranda
 */
public class Ecercici10 {

	public static void main(String[] args) {
		int num = 0;
		int num2 = 0;
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Introdueix un n�mero ");
		num = reader.nextInt();
		
		System.out.println("Introdueix un altre n�mero ");
		num2 = reader.nextInt();
		
		if (num != num2 && num > 0 && num2 > 0) {
			if (num > num2) {
				if (num % num2 == 0) {
					System.out.println(num + " �s m�ltpile de " + num2);
				} 
				else {
					System.out.println(num + " no �s m�ltple de " + num2);
				}
			}
		}
		reader.close();
	}
}

