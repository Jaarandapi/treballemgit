package provar;

import java.util.Scanner;

public class ExempleProvar {

	public static void main(String[] args) {
		
		int num = 0;
		Scanner reader = new Scanner(System.in);
		System.out.println("Introdueix un n�mero: ");
		num = reader.nextInt();
		if (num == 1)
		{
			System.out.println("Gener ");
		}
		else if (num == 2)
		{	
			System.out.println("Febrer ");
		}
		else if (num == 3)
		{
			System.out.println("mar� ");
		}	
		else if (num == 4)
		{
			System.out.println("abril ");
		}
		else if (num == 5)
		{
			System.out.println("maig ");
		}
		else if (num == 6)
		{
			System.out.println("juny ");
		}
		else if (num == 7)
		{
			System.out.println("juliol ");
		}
		else if (num == 8)
		{
			System.out.println("agost ");
		}
		else if (num == 9)
		{
			System.out.println("setembre ");
		}
		else if (num == 10)
		{
			System.out.println("octubre ");
		}
		else if (num == 11)
		{
			System.out.println("novembre ");
		}
		else if (num == 12)
		{
			System.out.println("desembre ");
		}
		else
		{
			System.out.println("incorrecte ");
		}
	}

}
